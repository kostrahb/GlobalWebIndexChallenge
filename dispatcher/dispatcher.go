// Dispatcher is module which is focused on work distribution to workers (it simulates a threadpool)
// Each dispatcher is created with given number of workers and publicly exposes its joq queue for communication with the world
package dispatcher


import(
	"gitlab.com/kostrahb/GlobalWebIndexChallenge/worker"
)

// Dispatcher is main structure for work dispatcher, it holds all information needed for work distribution and worker signaling
type Dispatcher struct {
	// Channel that we can send work requests on.
	JobQueue chan worker.Job
	// Signaling of exit
	exit chan bool
	// A pool of workers channels that are registered with the dispatcher
	workerPool chan chan worker.Job
	workers []worker.Worker
}


// NewDispatcher creates dispachter with given number of workers
func NewDispatcher(workers int) *Dispatcher {
	jobQueue := make(chan worker.Job)
	exit := make(chan bool)
	workerPool := make(chan chan worker.Job, workers)
	d := &Dispatcher{workerPool: workerPool, exit: exit, JobQueue:jobQueue}

	for i := 0; i < workers; i++ {
		w := worker.NewWorker(d.workerPool)
		d.workers = append(d.workers, w)
	}

	return d
}


// Start all workers and listen on queue for work
func (d *Dispatcher) Start() {
	for _, w := range(d.workers) {
		w.Start()
	}
	go d.dispatch()
}


func (d *Dispatcher) dispatch() {
	for {
		select {
		case job := <-d.JobQueue:
			// a job request has been received
			go func(job worker.Job) {
				// try to obtain a worker job channel that is available.
				// this will block until a worker is idle
				jobChannel := <-d.workerPool

				// dispatch the job to the worker job channel
				jobChannel <- job
			}(job)
		case <-d.exit:
			return
		}
	}
}


// Stop dispatcher and all workers
func (d *Dispatcher) Stop() {
	go func() {
		d.exit <- true
		for _, w := range(d.workers) {
			w.Stop()
		}
	}()
}
