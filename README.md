# About

This is a git repository which contains a task by Global Web Index -- a part of recruitment process.


# Running

To start the server it is sufficient to run command below. However it is possible to set the number of workers in environment variable `MAX_WORKERS`

```
go run server.go
```

# Testing 

There are two types of tests, unit tests which test some modules. You can run the unit tests in following way:

```
go test ./...
```

Then there is a test to check if the server is working as expected. If the server is already running, you can check by

```
go run test/test.go
```

# Benchmarking

Due to problems with test program (e.g. it would be probably hard to specify maximum concurent requests) the benchmarking might be done by ab - Apache HTTP server benchmarking tool:

```
ab -k -c 1000 -n 100000 -p data.json -T application/json "http://127.0.0.1:8080/challenge"
```

It might be desirable to tweak the values, mainly maximum concurent connections (`-c` flag) and total number of requests (`-n` flag)

# Profiling

It is easy to profile the server and see which functions are most time consuming. Pprof in webserver is included, so it is sufficient to run

```
go tool pprof server http://127.0.0.1:8080/debug/pprof/profile
```

And after obtaining the data command `web` or `top`

Note: The benchmark needs to run before/during profiling to obtain some traffic. Otherwise all goroutines are just waiting and profiling gets nothing useful.

# Docker

Docker image might be constructed from this application and sent to docker swarm or kubernetes cluster. To build image, issue:

```
make
```

After the image is built, the real fun begins. You can run multiple instances and do autoscaling in kubernetes. But let's be brief here - start the container like this:

```
docker run -p 8080:8080 challenge
```

Now test that it is accessible and in another terminal run

```
go run test/test.go
```

# Remarks

1. The main algorithm might not be very efficient due to things listed bellow. However with the small json like in the tests, the main problem is marshaling and unmarshaling (which can be seen from profiling). Therefore I would suggest moving from json to something else, the protocol buffers come to my mind for example. Or even go deeper move from http to gRPC.
  1. If the tree is deep, the performance might be significantly reduced by recursion used in tree methods.
  2. If the tree contains nodes with many children, the performance might be significantly reduced by sorting of children. This shouldn't be problem as sorting might be simply deleted. It's there just for predictability of tests (go randomizes map iteration).
  3. The tree representation is really handy for understanding however it requires three functions (conversion to tree, calculation of values and conversion back to array) which might be potentially merged together.
