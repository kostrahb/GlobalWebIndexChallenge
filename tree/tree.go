// Package tree is an implementation of basic tree structure.
package tree


import(
	"fmt"
	"reflect"
	"sort"
)


// Tree structure represents general tree
type Tree struct {
	children []*Tree
	value float64
	name string
}


// Print prints all nodes, their children, values and names
func (t *Tree) Print() {
	fmt.Printf("%v\n", t)
	for _, c := range(t.children) {
		c.Print()
	}
}


// SumOfLeaves calculates value of the node as a sum of values of its children. Recursively
func (t *Tree) SumOfLeaves() {
	if len(t.children) > 0 {
		var sum float64
		sum = 0
		for _, c := range(t.children) {
			c.SumOfLeaves()
			sum += c.value
		}
		t.value = sum
	}
}


// ToMap traverses given tree and adds each node to a map. The key is node name and value is node value.
func (t *Tree) ToMap() ([]map[string]float64) {
	r := []map[string]float64{}
	m := make(map[string]float64)

	// Treatment for non root node
	if t.name != "" {
		m[t.name] = t.value
		r = append(r, m)
	}


	for _, c := range t.children {
		a := c.ToMap()
		r = append(r, a...)
	}
	return r
}


// NewTree converts nested map (on each level there is either map[string]map or map[string]int) to structure representing a tree
// If it cannot retrieve the value, it leaves zero there
func NewTree(name string, data map[string]interface{}) (*Tree) {
	t := &Tree{children:[]*Tree{}, value:0, name:name}

	if count, ok := data["count"]; ok {
		// Create a leaf
		rv := reflect.ValueOf(count)
		switch rv.Kind() {
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			t.value = float64(rv.Int())
		case reflect.Uint, reflect.Uintptr, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			t.value = float64(rv.Uint())
		case reflect.Float32, reflect.Float64:
			t.value = rv.Float()
		}
		return t
	}

	for key, val := range data {
		if v, ok := val.(map[string]interface{}); ok {
			t.children = append(t.children, NewTree(key, v))
		}
	}

	// WARNING: Sorting might be very time consuming, especially with many children, but it helps with predictable output for testing
	sort.SliceStable(t.children, func(i, j int) bool { return t.children[i].name < t.children[j].name })
	return t
}
