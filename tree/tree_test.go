package tree

import(
	"fmt"
	"testing"
	"reflect"
)


func TestSumOfLeaves(t *testing.T) {
	tree1 := &Tree{children:[]*Tree{
			&Tree{children:[]*Tree{}, value:10, name:""},
			&Tree{children:[]*Tree{}, value:5, name:""},
		}, value:0, name:""}
	tree2 := &Tree{children:[]*Tree{
			&Tree{children:[]*Tree{}, value:10, name:""},
			&Tree{children:[]*Tree{}, value:5, name:""},
		}, value:15, name:""}
	tree1.SumOfLeaves()

	if !reflect.DeepEqual(tree1, tree2) {
		tree1.Print()
		tree2.Print()
		t.Errorf("The structures don't match")
	}
}

func TestToMap(t *testing.T) {
	tree := &Tree{children:[]*Tree{
			&Tree{children:[]*Tree{}, value:10, name:"ONE"},
			&Tree{children:[]*Tree{}, value:5, name:"TWO"},
		}, value:0, name:""}
	expected := []map[string]float64{map[string]float64{"ONE":10}, map[string]float64{"TWO":5}}
	m := tree.ToMap()
	if !reflect.DeepEqual(expected, m) {
		t.Errorf("The structures don't match: \n%v\n%v", m, expected)
	}
}


func TestNewTree(t *testing.T) {
	m := &map[string]interface{}{"ONE": *&map[string]interface{}{"TWO": *&map[string]interface{}{"count":10.0}, "THREE": *&map[string]interface{}{"count":5.0}}}

	expected := &Tree{children:[]*Tree{
			&Tree{children:[]*Tree{
				&Tree{children:[]*Tree{}, value:5, name:"THREE"},
				&Tree{children:[]*Tree{}, value:10, name:"TWO"},
			}, value:0, name:"ONE"}},
		value:0, name:""}
	tree := NewTree("", *m)
	if !reflect.DeepEqual(expected, tree) {
		tree.Print()
		fmt.Println()
		expected.Print()
		fmt.Println()
		t.Errorf("The structures don't match")
	}
}
