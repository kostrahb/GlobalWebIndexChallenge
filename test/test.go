package main

import (
	"bytes"
	"encoding/json"
	"reflect"
	"net/http"
	"strings"
	"fmt"
	"log"
	"os"
)

var (
	testBody = `{"data": {"facet1": {"facet3": {"facet4": {"facet6": {"count": 20},"facet7": {"count":
30}},"facet5": {"count": 50}}}, "facet2": {"count": 0}}}`
	expectedOutput = `{"result":[{"facet1":100},{"facet3":100},{"facet4":50},{"facet6":20},{"facet7":30},{"facet5":50},{"facet2":0}]}`
)

func AreEqualJSON(s1, s2 string) (bool, error) {
	var o1 interface{}
	var o2 interface{}

	var err error
	err = json.Unmarshal([]byte(s1), &o1)
	if err != nil {
		return false, fmt.Errorf("Error mashalling string 1 :: %s", err.Error())
	}
	err = json.Unmarshal([]byte(s2), &o2)
	if err != nil {
		return false, fmt.Errorf("Error mashalling string 2 :: %s", err.Error())
	}

	return reflect.DeepEqual(o1, o2), nil
}

func main() {
	for i := 0; i<1000; i++ {
		res, err := http.Post("http://localhost:8080/challenge", "application/json; charset=utf-8", strings.NewReader(testBody))
		if err != nil {
			log.Fatal(err)
		}
		buf := new(bytes.Buffer)
		buf.ReadFrom(res.Body)
		s := buf.String()
		equal, err := AreEqualJSON(s, expectedOutput)
		if err != nil {
			fmt.Println("Error while investigating if two jsons are equal!")
		}
		if !equal {
			fmt.Printf("Error! Got\n%v\n, expected\n%v", s, expectedOutput)
			os.Exit(1)
		}
	}
	fmt.Println("done")
}
