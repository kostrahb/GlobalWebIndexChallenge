docker-build:
	docker rmi challenge --force
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
	docker build -t challenge -f Dockerfile .
