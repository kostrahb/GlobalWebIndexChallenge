package main


import(
	"os"
	"os/signal"

	"fmt"
	"log"

	"syscall"

	"strconv"
	"encoding/json"

	"net/http"
	// Profiling
	_ "net/http/pprof"

	"gitlab.com/kostrahb/GlobalWebIndexChallenge/tree"
	"gitlab.com/kostrahb/GlobalWebIndexChallenge/dispatcher"
)


var (
	MaxWorker int
	d *dispatcher.Dispatcher
)


type in struct {
	Data interface{} `json:"data"`
}

type out struct {
	Result interface{} `json:"result"`
}


func requestHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	done := make(chan bool)

	// Main procedure for processing data
	work := func() {
		// Decode body of request from json to struct
		var content = &in{}
		err := json.NewDecoder(r.Body).Decode(&content)
		if err != nil {
			w.WriteHeader(http.StatusUnprocessableEntity)
		}

		// Convert structure to tree
		t := tree.NewTree("", content.Data.(map[string]interface{}))
		// Calculate sum of all descendant leaves
		t.SumOfLeaves()
		// Convert back to map (different one)
		m := t.ToMap()

		o := out{m}

		// Marshal it back to json
		s, err := json.Marshal(o)
		if err != nil {
			w.WriteHeader(http.StatusUnprocessableEntity)
		}
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")

		w.WriteHeader(http.StatusOK)
		w.Write(s)
		done <- true
	}

	// Push the work onto the queue.
	d.JobQueue <- work
	// Wait for function to complete (needs to be here so that the )
	<-done
}


func main() {
	MaxWorker, _ = strconv.Atoi(os.Getenv("MAX_WORKERS"))
	if MaxWorker == 0 {
		MaxWorker = 4
	}

	d = dispatcher.NewDispatcher(MaxWorker)
	d.Start()

	http.HandleFunc("/challenge", requestHandler)

	gracefulStop := make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	go func() {
		<-gracefulStop
		fmt.Println("\b\bExitting server")
		d.Stop()
		os.Exit(0)
	}()

	log.Fatal(http.ListenAndServe(":8080", nil))
}
